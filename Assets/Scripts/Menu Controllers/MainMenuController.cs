﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
//using System.Runtime.Hosting;

public class MainMenuController : MonoBehaviour {

	public void PlayGame() {
        AudioManager.instance.PlaySFX4();
        SceneManager.LoadScene("GamePlay");
    }

    public void Settings()
    {
        AudioManager.instance.PlaySFX4();
        SceneManager.LoadScene("Setting");
    }

    public void Exit()
    {
        AudioManager.instance.PlaySFX4();
        Application.Quit();
    }
}
