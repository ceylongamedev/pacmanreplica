﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayController : MonoBehaviour {

	public Animator GameOverAnim;
	public GameObject[] GameOverMessage;

	public Image pauseButton;

	public Sprite pause;
	public Sprite resume;

	private bool didFunctionMM = false;
	private bool didFunctionRS = false;

	public void RestartGame() {

		AudioManager.instance.PlaySFX3();

		Debug.Log("Restart");

		didFunctionRS = true;

		for (int i = 0; i < GameOverMessage.Length; i++)
		{
			GameOverMessage[i].SetActive(false);
		}
		GameOverAnim.Play("GameOverEnd");
		Invoke("PanelActive", 1f);
		
	}

	public void MainMenu()
	{
		AudioManager.instance.PlaySFX4();

		Debug.Log("Main Menu");
		didFunctionMM = true;

		for (int i = 0; i < GameOverMessage.Length; i++)
		{
			GameOverMessage[i].SetActive(false);
		}
		GameOverAnim.Play("GameOverEnd");

		Invoke("PanelActive", 1f);
	}

	void PanelActive()
	{
		if (didFunctionMM == true)
		{
			SceneManager.LoadScene(0);
		}
		else if(didFunctionRS == true)
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}		
	}

	public void puse()
	{
		AudioManager.instance.PlaySFX3();
		if (Time.timeScale == 1)
		{
			Time.timeScale = 0;
			pauseButton.sprite = resume;
		}
		else
		{
			Time.timeScale = 1;
			pauseButton.sprite = pause;
		}
	}
}
