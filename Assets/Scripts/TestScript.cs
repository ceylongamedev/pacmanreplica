﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TestScript : MonoBehaviour {

	public float speed = 0.3f;

	private Vector2 destination;
	private Vector2 nextDirection;

	private Rigidbody2D _rBody;
	private Animator anim;

	public Animator GameEndAnim;
	public GameObject[] GameOverMessage;
	public GameObject GameOverPanel;

	private enum CurrentDirection {left, right, up, down};
	private CurrentDirection dir;

	private CurrentDirection[] directions = {CurrentDirection.left, CurrentDirection.right, CurrentDirection.up, CurrentDirection.down};
	private bool canMove;

	void Awake() {
		_rBody = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
	}

	
	void Start () {
		destination = GetFirsDestinationForMonster ();

		if (Random.Range (0, 2) > 0) {
			dir = CurrentDirection.left;
			DetermineNextDirection ();
		} else {
			dir = CurrentDirection.right;
			DetermineNextDirection ();
		}

		canMove = true;

	}
	
	
	void FixedUpdate () {
		if (canMove) {
			Move();
			Animate();
		}
	}

	private Vector3 GetFirsDestinationForMonster() {
		switch (gameObject.name) {
		case "Blue Monster":
			return new Vector3(16, 20, 0);
			break;

		case "Green Monster":
			return new Vector3(14, 20, 0);
			break;

		case "Pink Monster":
			return new Vector3(13, 20, 0);
			break;

		default:
			return new Vector3(0, 0, 0);
			break;
		}
	}

	CurrentDirection GetRandomDirection() {
		return directions[Random.Range(0, directions.Length)];
	}

	private void DetermineNextDirection() {
		switch (dir) {
		case CurrentDirection.left:
			nextDirection = Vector2.left;
			break;

		case CurrentDirection.right:
			nextDirection = Vector2.right;
			break;

		case CurrentDirection.up:
			nextDirection = Vector2.up;
			break;

		case CurrentDirection.down:
			nextDirection = Vector2.down;
			break;
		}
	}	

	private void SetCurrentDirection() {
		canMove = false;

		CurrentDirection newDirection = GetRandomDirection ();

		while (newDirection == dir) {
			newDirection = GetRandomDirection ();
		}

		dir = newDirection;
		DetermineNextDirection ();

		canMove = true;
	}

	private void SetDestination() {
		destination = (Vector2)transform.position + nextDirection;
	}

	bool IsValidDirection(Vector2 dir) {
		Vector2 pos = transform.position;
		
		dir += new Vector2 (dir.x * 0.45f, dir.y * 0.45f);
		
		RaycastHit2D hit = Physics2D.Linecast (pos + dir, pos);
		
		if (hit.collider.tag == "Maze")
			return false;
		else
			return true;
		
	}

	void Move() {

		if ((Vector2)transform.position != destination) {
			Vector2 pos = Vector2.MoveTowards (transform.position, destination, speed);
			_rBody.MovePosition (pos);
		} else {
			if(IsValidDirection(nextDirection)) {
				SetDestination();
			} else {
				SetCurrentDirection();
			}
		}

	}

	void Animate() {
		Vector2 dir = destination - (Vector2)transform.position;
		anim.SetFloat ("X", dir.x);
		anim.SetFloat ("Y", dir.y);
	}

	void OnTriggerEnter2D(Collider2D target) {
		if (target.tag == "PacMan") {

			GameOverPanel.SetActive(true);
			GameEndAnim.Play("GameOver");
			Invoke("GameOver", 1f);
			Destroy(target.gameObject);
		}
	}

	void GameOver() {
		for (int i = 0; i < GameOverMessage.Length; i++)
		{
			GameOverMessage[i].SetActive(true);
		}
	}

}//class
















































