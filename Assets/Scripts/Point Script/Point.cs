﻿using UnityEngine;
using System.Collections;

public class Point : MonoBehaviour {


	void OnTriggerEnter2D(Collider2D target) {
		if (target.tag == "PacMan") {
			ScoreManager scoreManager = GameObject.FindGameObjectWithTag("Point").GetComponent<ScoreManager>();
			if(scoreManager != null)
			{
				//Debug.Log("one");
				scoreManager.score += 1;
			}
			Destroy(gameObject);
		}
	} 

}
