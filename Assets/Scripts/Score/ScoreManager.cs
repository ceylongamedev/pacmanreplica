﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Animator GameEndAnim;
    public GameObject[] GameOverMessage;
    public GameObject GameOverPanel;

    public Text Score;
    public Text GameWinText;
    public int score = 0;

    [SerializeField]
    private GameObject Points;


    private int ChildCount;

    private void Awake()
    {
        ChildCount = Points.transform.childCount;
    }
    void Update()
    {
        Score.text = "SCORE: " + score.ToString();

        if(score == ChildCount)
        {
            AudioManager.instance.PlaySFX5();
            score = 0;
            Debug.Log("You Win");
            if(GameObject.FindGameObjectWithTag("PacMan") != null)
            {
                GameObject.FindGameObjectWithTag("PacMan").SetActive(false);
            }
            GameOverPanel.SetActive(true);
            GameEndAnim.Play("GameOver");
            Invoke("GameOver", 1f);

        }

    }

    void GameOver()
    {
        for (int i = 0; i < GameOverMessage.Length; i++)
        {
            GameOverMessage[i].SetActive(true);
        }
        GameWinText.text = "YOU WIN";
    }
}
