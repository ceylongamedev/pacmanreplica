﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static AudioManager instance;

    AudioSource ButtonSFX;
    public AudioClip SFX1;
    public AudioClip SFX2;
    public AudioClip SFX3;
    public AudioClip SFX4;
    public AudioClip SFX5;
    public AudioClip SFX6;

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        ButtonSFX = GetComponent<AudioSource>();
    }

    public void PlaySFX1()
    {
        ButtonSFX.PlayOneShot(SFX1);
    }

    public void PlaySFX2()
    {
        ButtonSFX.PlayOneShot(SFX2);
    }

    public void PlaySFX3()
    {
        ButtonSFX.PlayOneShot(SFX3);
    }

    public void PlaySFX4()
    {
        ButtonSFX.PlayOneShot(SFX4);
    }

    public void PlaySFX5()
    {
        ButtonSFX.PlayOneShot(SFX5);
    }

    public void PlaySFX6()
    {
        ButtonSFX.PlayOneShot(SFX6);
    }


}
